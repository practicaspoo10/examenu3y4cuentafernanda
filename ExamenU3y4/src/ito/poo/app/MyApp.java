package ito.poo.app;

import ito.poo.clases.Cuenta;

import java.time.LocalDate;

import ito.poo.clases.Ahorro;
import ito.poo.clases.Credito;

public class MyApp {
	
	static void run() {
		
		Cuenta c= new Cuenta(451516161, "Sarai Toledo", 48484494);
		Credito cd= new Credito (451516161, "Sarai Toledo", 48484494, 100000f, 5210f);
		Ahorro a= new Ahorro(451516161, "Sarai Toledo", 48484494, 8691f, LocalDate.of(2021, 10, 15), 12546, 120);
		
		System.out.println(c);
		System.out.println(cd);
		System.out.println(a);
		
	}
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		run();

	}

}
